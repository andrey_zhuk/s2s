<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Reset Password';

$this->params['breadcrumbs'][] = ['label'=>$this->title];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="panel col-md-6 col-md-offset-3">
             <div class="panel-body">
                <h1 class="text-center">Password Reset</h1>
                <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($model, 'password')->passwordInput()->label('New Password') ?>
                        <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                        </div>
                <?php ActiveForm::end(); ?>
             </div>
        </div>
    </div>
</div>
