<?php

use yii\helpers\Html;

$this->title = 'S2S Tracking Pixel';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome To Tracking Pixel</h1>
        <?= Yii::$app->user->isGuest ? ('<p class="lead">Login in as admin and check tracking of pixels.</p>') :
            (Html::a('Add Pixel', ['/site/pixel'], ['class' => 'btn btn-lg btn-success'])); ?>
    </div>

    <div class="body-content">

    </div>
</div>
