<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'List Pixel';

?>

<div class="site-index">

    <div class="jumbotron">
        <h1>Pixel Tracking</h1>

        <p class="lead">To create new pixel click the below button.</p>

        <p><?= Html::a('Add Pixel', ['/site/pixel'], ['class'=>'btn btn-lg btn-success']) ?></p>
        
   </div>

    <div class="body-content">
    	<?php echo GridView::widget([
		  'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
		]); ?>
    </div>
</div>
