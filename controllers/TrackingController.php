<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Tracking;
use app\models\TrackingSearch;
use yii\data\ActiveDataProvider;

class TrackingController extends Controller
{
	/**
     * Displays tracking.
     *
     * @return string
     */
    public function actionTracklist()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $searchModel = new TrackingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('tracklist', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}