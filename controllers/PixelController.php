<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Pixel;
use app\models\PixelSearch;
use yii\data\ActiveDataProvider;

class PixelController extends Controller
{
	/**
     * Displays tracking.
     *
     * @return string
     */
    public function actionPixellist()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $searchModel = new PixelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        /*$query = Pixel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC, 
                ]
            ],
        ]);*/
        return $this->render('pixellist', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}