<?php

namespace app\controllers;

use app\models\Pixel;
use app\models\ResetPasswordForm;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\PixelForm;
use app\models\Tracking;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $get = Yii::$app->request->get();

        if (isset($get['siteid']) && $get['siteid']) {

            if (isset($get['siteid']) && $get['uid']) {

                $data = Pixel::find()->andWhere(['site_id' => $get['siteid']])->count();

                if ($data > 0) {
                    $tracking = new Tracking();
                    ($get['siteid']) ? $tracking->site_id = $get['siteid'] : 'NA';
                    ($get['site']) ? $tracking->site = $get['site'] : 'NA';
                    ($get['uid']) ? $tracking->unique_id = $get['uid'] : 'NA';
                    ($get['clickid']) ? $tracking->click_id = $get['clickid'] : 'NA';
                    ($get['clientid']) ? $tracking->client_id = $get['clientid'] : 'NA';
                    ($get['action']) ? $tracking->action = $get['action'] : 'NA';
                    ($get['device']) ? $tracking->device = $get['device'] : 'NA';
                    ($get['channel']) ? $tracking->channel = $get['channel'] : 'NA';

                    // Server variables
                    $tracking->ip_addr = $_SERVER['REMOTE_ADDR'];
                    $tracking->host = $_SERVER['HTTP_HOST'];
                    $tracking->user_agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';

                    $tracking->save();
                }
            }
            die;
        } else {
            return $this->render('index');
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLoginh64bpc()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Pixel action.
     *
     * @return Response|string
     */
    public function actionPixel()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new PixelForm();
        if ($model->load(Yii::$app->request->post()) && $model->pixel()) {
            return $this->redirect(['pixel/pixellist']);
        }
        return $this->render('pixel', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword()
    {

        try {
            $model = new ResetPasswordForm();
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'New password was saved.'));

            return $this->redirect('/');
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
