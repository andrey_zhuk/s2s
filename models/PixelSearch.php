<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PixelSearch extends Pixel
{
    public function rules()
    { 
        // only fields in rules() are searchable
        return [
            [['company_name'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Pixel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['like', 'company_name', $this->company_name]);

        return $dataProvider;
    }
}