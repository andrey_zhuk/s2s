<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * PixelForm is the model behind the pixel form.
 */
class PixelForm extends Model
{
    public $company_name;
    public $website_url;
    
        private $pixel_url;
        private $site_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // company_name and website_url are both required
            [['company_name', 'website_url'], 'required'],
        ];
    }

    /**
     * Creates a pixel using the provided information.
     * @return bool whether the pixel is created successfully
     */
    public function pixel()
    {
        if ($this->validate()) {
            $pixel = new Pixel();
            $pixel->company_name = $this->company_name;
            $pixel->website_url = $this->website_url;
            $pixel->site_id = $this->getUniqueClientId();

            $url = Yii::$app->urlManager->createAbsoluteUrl(['site', 'siteid' => $pixel->site_id, 'site' => '{url}', 'action' => '{action}', 'clickid' => '{clickid}', 'clientid' => '{clientid}', 'device' => '{device}', 'channel' => '{channel}', 'uid' => '{uniqueid}']);
            $pixel->pixel_url = '<img src="'.urldecode ( $url ).'" style="display:none">';
            
            $pixel->save();

            return true;
        }
        return false;
    }

    /**
     * @return string the uniqueid used for client.
     */
    public function getUniqueClientId()
    {
        return  substr(md5(uniqid(rand(), true)), 0, 10);
    }
}
