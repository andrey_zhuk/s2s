<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class TrackingSearch extends Tracking
{
    public function rules()
    { 
        // only fields in rules() are searchable
        return [
            [['click_id', 'client_id'], 'integer'],
            [['site_id', 'action', 'device', 'channel'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Tracking::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        $query->andFilterWhere(['click_id' => $this->click_id])
              ->andFilterWhere(['client_id' => $this->client_id]);
        $query->andFilterWhere(['like', 'site_id', $this->site_id])
              ->andFilterWhere(['like', 'action', $this->action])
              ->andFilterWhere(['like', 'device', $this->device])
              ->andFilterWhere(['like', 'channel', $this->channel]);

        return $dataProvider;
    }
}